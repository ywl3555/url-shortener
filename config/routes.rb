Rails.application.routes.draw do
  # Root path to the form for creating a new short URL
  root 'short_urls#new'

  # Resources for short URLs to handle creation and listing
  resources :short_urls, only: [:new, :create, :index]

  # Redirect route for the short path
  get '/:short_path', to: 'short_urls#redirect', as: :short_url

  # Stats route for viewing clicks statistics
  get '/short_urls/:short_path/click_logs', to: 'short_urls#click_logs', as: 'short_url_click_logs'
end
