require 'rails_helper'

RSpec.describe ShortUrlsController, type: :controller do
  let(:valid_attributes) {
    { original_url: "https://example.com", short_path: "exmpl" }
  }

  let(:invalid_attributes) {
    { original_url: nil, short_path: nil }
  }

  describe "GET #redirect" do
    context "when short URL exists" do
      it "redirects to the original URL" do
        original_url = OriginalUrl.create!(url: valid_attributes[:original_url])
        short_url = ShortUrl.create!(original_url: original_url, short_path: valid_attributes[:short_path])
        get :redirect, params: { short_path: short_url.short_path }
        expect(response).to redirect_to(short_url.original_url.url)
      end
    end

    context "when short URL does not exist" do
      it "returns a 404 response" do
        get :redirect, params: { short_path: "nonexistent" }
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "GET #click_logs" do
    context "when short URL exists" do
      before do
        original_url = OriginalUrl.create!(url: valid_attributes[:original_url])
        short_url = ShortUrl.create!(original_url: original_url, short_path: valid_attributes[:short_path])
        get :click_logs, params: { short_path: short_url.short_path }
      end

      it "renders the click_logs template" do
        expect(response).to render_template(:click_logs)
      end
    end

    context "when short URL does not exist" do
      before do
        get :click_logs, params: { short_path: "nonexistent" }
      end

      it "returns a not found status" do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "GET #new" do
    it "renders the new template" do
      get :new
      expect(response).to render_template(:new)
    end

    it "assigns a new short_url and all short_urls to @short_url and @short_urls" do
      original_url = OriginalUrl.create!(url: valid_attributes[:original_url])
      valid_attributes[:original_url] = original_url
      short_url = ShortUrl.create! valid_attributes
      get :new
      expect(assigns(:short_url)).to be_a_new(ShortUrl)
      expect(assigns(:short_urls)).to eq([short_url])
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new ShortUrl and redirects" do
        expect {
          post :create, params: { short_url: valid_attributes }
        }.to change(ShortUrl, :count).by(1)
        expect(response).to redirect_to(new_short_url_path)
      end
    end

    context "with invalid params" do
      it "does not create a new ShortUrl and rerenders the new template" do
        expect {
          post :create, params: { short_url: invalid_attributes }
        }.to change(ShortUrl, :count).by(0)
      end
    end
  end

  describe "GET #index" do
    it "renders the index template" do
      get :index
      expect(response).to render_template(:index)
    end
  end
end
