require 'rails_helper'

RSpec.describe ShortUrl, type: :model do
  describe 'associations' do
    it { should belong_to(:original_url) }
    it { should have_many(:click_logs) }
  end

  describe 'validations' do
    subject { ShortUrl.new(original_url: OriginalUrl.new(url: 'http://example.com')) }

    it { should validate_uniqueness_of(:short_path) }
    it { should validate_length_of(:short_path).is_at_most(15) }
  end

  describe '#generate_short_path' do
    it 'generates a unique short path if none provided' do
      original_url = OriginalUrl.create!(url: 'https://example.com')
      short_url = original_url.short_urls.create!
      expect(short_url.short_path).not_to be_nil
    end

    it 'does not overwrite an existing short path' do
      original_url = OriginalUrl.create!(url: 'https://example.com')
      existing_path = 'existing123'
      short_url = original_url.short_urls.create!(short_path: existing_path)
      expect(short_url.short_path).to eq(existing_path)
    end
  end

  describe '#generate_unique_short_path' do
    it 'avoids duplicate short paths' do
      original_url = OriginalUrl.create!(url: 'https://example.com')
      existing_short_url = ShortUrl.create!(original_url: original_url, short_path: 'unique123')
      allow(Base58).to receive(:encode).and_return('unique123', 'unique123', 'unique124')
      new_short_url = original_url.short_urls.create!
      expect(new_short_url.short_path).not_to eq(existing_short_url.short_path)
    end
  end
end
