require 'rails_helper'

RSpec.describe ClickLog, type: :model do
  describe 'associations' do
    it { should belong_to(:short_url) }
  end
end
