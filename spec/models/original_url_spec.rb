require 'rails_helper'

RSpec.describe OriginalUrl, type: :model do
  describe 'associations' do
    it { should have_many(:short_urls) }
  end

  describe 'validations' do
    it { should validate_presence_of(:url) }
  end

  describe 'valid url' do
    it 'is valid with a proper URL' do
      expect(OriginalUrl.new(url: 'https://example.com')).to be_valid
    end

    it 'is not valid without a URL' do
      expect(OriginalUrl.new(url: nil)).not_to be_valid
    end
  end
end
