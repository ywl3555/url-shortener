class CreateShortUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :short_urls do |t|
      t.references :original_url, null: false, foreign_key: true
      t.string :short_path

      t.timestamps
    end
    add_index :short_urls, :short_path, unique: true
  end
end
