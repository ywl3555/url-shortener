class CreateClickLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :click_logs do |t|
      t.references :short_url, null: false, foreign_key: true
      t.string :ip_address
      t.datetime :clicked_at

      t.timestamps
    end
  end
end
