class ChangeShortPathCollationToCaseSensitive < ActiveRecord::Migration[6.0]
  def up
    execute "ALTER TABLE short_urls MODIFY short_path VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin"
  end

  def down
    execute "ALTER TABLE short_urls MODIFY short_path VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci"
  end
end
