class CreateOriginalUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :original_urls do |t|
      t.string :url

      t.timestamps
    end
    add_index :original_urls, :url, unique: true
  end
end
