# URL Shortener Project

### Application URL:
<https://url-shortener-snowy-morning-2811.fly.dev/>

## Introduction

This project is a URL shortener service built using Ruby on Rails and Tailwind CSS. The service allows users to create shortened URLs that redirect to longer target URLs. The application tracks analytics for each shortened URL, recording the IP address and timestamp of each click. This service supports the creation of multiple short URLs for the same target URL and provides a basic report endpoint to check the click statistics for each short URL.

## Assumptions

### Project Scope

> You are tasked to build a simple URL Shortener service as a microservice for a startup you recently joined.
> A URL Shortener service, similar to bit.ly and tinyurl.com is a service that maps a short-form URL ("Short URL") to a
> user-provided target URL ("Target URL").'

> - Your application is deployed with a web interface and a form field that accepts a Target URL.
> - When the Target URL is shortened, the user is returned with a Short URL, the original Target URL and the Title tag of the Target URL.
> - A Short URL can be publicly shared and accessed.
> - A Short URL path can be in any URI pattern, but should not exceed a maximum of 15 characters
> - Multiple Short URLs can share the same Target URL.
> - You need to produce a simple usage report for the application. This report should track the number of clicks, originating geolocation and timestamp of each visit to a Short URL.

### Traffic Volume

- **Daily Short URL Creation**: 1,000 short URLs will be created daily.
- **Read/Write Ratio**: The read-to-write ratio will be 10:1, meaning for every write operation (URL creation), there will be 10 read operations (URL clicks).

### Storage Requirement Calculation

To estimate how many short URLs the application should store over a 10-year period:

- **Daily URL Creation**: 1,000 short URLs
- **Yearly URL Creation**: 1,000 short URLs/day * 365 days/year = 365,000 short URLs/year
- **10-Year URL Creation**: 365,000 short URLs/year * 20 years = 7,300,000 short URLs

Therefore, over a 10-year period, the application should be able to store approximately 7.3 millions short URLs.

## Short Path Generation

### Base58 Encoding

The application generates the short path using Base58 encoding. This encoding method is chosen to avoid characters that can be visually confusing (such as '0', 'O', 'I', and 'l'), so that they can get rid of typo even if they are typing the link by themselves. Base58 includes the following characters:

`123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz`


### Generation Process

1. **Random Number Generation**: A random number is generated.
2. **Base58 Encoding**: The random number is encoded into a Base58 string.
3. **Collision Handling**: The first 8 characters of the Base58 string are used as the short path. If this short path already exists in the database, the number is incremented, and the encoding process is repeated until a unique short path is found.

It’s not possible to figure out the next available short URL because it doesn’t depend on ID, therefore enhance the security of the application.

### Capacity Calculation

The total number of possible unique short paths with 8 characters using Base58 encoding is:

`58^8 = approx 2,200,000,000,000,000`

This vast number of combinations (approximately 2.2 quadrillion) is more than sufficient to handle the projected traffic volume of 7.3 millions short URLs over 20 years.


## Features

### URL Shortening

- **Short URL Generation**: Users can input a long URL and receive a shortened URL. Additionally, users have the option to customize their own short path or let the app generate it automatically. For analytical purposes, multiple short links can belong to the same target link. For example, different shortened URLs can be created for the same target link and shared in different places to see which has higher click rates.

- **Short URL Redirection**: Short URLs redirect to the original long URL.

- **Visit Tracking**: The application tracks analytics for each shortened URL, recording the IP address and timestamp of each click. This service supports the creation of multiple short URLs for the same target URL and provides a basic report endpoint to check the click statistics for each short URL

## Screenshots

![Screenshot 1](/images/screenshot-1.png "Screenshot 1")
![Screenshot 2](/images/screenshot-2.png "Screenshot 2")

## Table Design

- **OriginalUrls Table**:
  - `id`: Primary key
  - `url`: The original long URL
  - `created_at`: Timestamp for when the record was created
  - `updated_at`: Timestamp for when the record was last updated

- **ShortUrls Table**:
  - `id`: Primary key
  - `original_url_id`: Foreign key referencing the OriginalUrls table
  - `short_path`: The shortened path
  - `created_at`: Timestamp for when the record was created
  - `updated_at`: Timestamp for when the record was last updated

- **ClickLogs Table**:
  - `id`: Primary key
  - `short_url_id`: Foreign key referencing the ShortUrls table
  - `ip_address`: IP address of the user clicking the short URL
  - `clicked_at`: Timestamp for when the click occurred
 
## Tools Used

- **Ruby on Rails**: Framework for building the application
- **Rspec**: Testing framework for Ruby
- **Tailwind CSS**: Utility-first CSS framework for styling
- **MySQL**: Database for storing data
- **Gitlab**: Version control and CI (Automated testing for every PRs)
- **Fly.io**: Platform for deploying the application

## Further Improvements

- **Caching**: Implement caching for hot URL links, such as those created by celebrities or large organizations, to reduce database load and improve performance.
- **Indexing**: Add indexes to the database tables to speed up queries and improve read performance.
- **Master-Slave Database Configuration**: Set up a master-slave database configuration to handle higher traffic volumes and improve read performance.
- **Rate Limiting**: Implement rate limiting to prevent abuse of the service and ensure fair usage.
- **Geolocation Tracking**: Since we already record the IP address for every log entry, we can enhance our geolocation tracking feature by looking up the location of each IP address. This will provide us with more detailed information about the geographical location of users clicking on short URLs, such as their city, region, country, and possibly even their internet service provider (ISP). By integrating an IP geolocation service, we can gain valuable insights into user demographics and behavior, allowing us to tailor our services and marketing efforts more effectively.
- **QR Code Generation**: Add the ability to generate QR codes for short URLs to facilitate sharing and tracking.
- **User Authentication**: Implement user authentication to allow users to manage their short URLs and view detailed analytics.
