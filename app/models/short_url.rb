class ShortUrl < ApplicationRecord
  belongs_to :original_url
  has_many :click_logs

  validates :short_path, length: { maximum: 15 }, uniqueness: true

  before_validation :generate_short_path, on: :create

  private

  def generate_short_path
    self.short_path = generate_unique_short_path if self.short_path.blank?
  end

  def generate_unique_short_path
    loop do
      random_path = Base58.encode(SecureRandom.random_number(2147483647)).first(8)
      break random_path unless ShortUrl.exists?(short_path: random_path)
    end
  end
end
