class OriginalUrl < ApplicationRecord
  has_many :short_urls

  validates :url, presence: true
end
