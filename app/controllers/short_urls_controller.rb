class ShortUrlsController < ApplicationController
  def redirect
    @short_url = ShortUrl.find_by(short_path: params[:short_path])
  
    if @short_url.nil?
      render file: "#{Rails.root}/public/404.html", status: :not_found
    else
      ClickLog.create!(short_url: @short_url, ip_address: request.remote_ip, clicked_at: Time.now)
      redirect_to @short_url.original_url.url
    end
  end
  
  def click_logs
    @short_url = ShortUrl.find_by(short_path: params[:short_path])
    @click_logs = @short_url.click_logs if @short_url
    @original_url = @short_url.original_url.url if @short_url
    @total_clicks = @click_logs.count if @click_logs

    render :click_logs, status: @short_url ? :ok : :not_found
  end
  
  def new
    @short_url = ShortUrl.new
    @short_urls = ShortUrl.all.order(created_at: :desc).includes(:click_logs)
  end

  def create
    return if short_url_params[:original_url].blank?

    @short_url = build_short_url_from_params

    if @short_url.save
      redirect_to new_short_url_path, notice: 'Short URL created successfully.'
    else
      @short_urls = ShortUrl.all.order(created_at: :desc).includes(:click_logs)
      flash.now[:error] = 'Failed to create Short URL.'
      render :new
    end
  end

  def index
    @short_urls = ShortUrl.all
  end
  
  private
  
  def build_short_url_from_params
    original_url = OriginalUrl.find_or_create_by!(url: short_url_params[:original_url])
    original_url.short_urls.build(short_path: short_url_params[:short_path])
  rescue ActiveRecord::RecordInvalid => e
    flash[:error] = e.message
    nil
  end
  
  def short_url_params
    params.require(:short_url).permit(:original_url, :short_path)
  end  
end
